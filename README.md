# GAF Examples #

This code should be used in conjunction with the related documentation at [http://johnnewcombe.net/gaf](http://johnnewcombe.net/gaf).

In addition to the source posted here, executable binaries are available within the a single Docker image available from Docker Hub.
[https://hub.docker.com/r/johnnewcombe/gafexamples](https://hub.docker.com/r/johnnewcombe/gafexamples).
﻿using System;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using GAF.Net;

namespace RemoteEvaluationClient
{
	public class Parameters
	{
		private const int defaultPort = 11000;

		public Parameters (String[] args)
		{
			Endpoints = new List<IPEndPoint> ();

			if (args != null) {

				var paramEps = args
					.Where (arg => arg.StartsWith ("-ep", 
						StringComparison.InvariantCultureIgnoreCase)).ToList ();

				//Endpoint IP Address
				if (paramEps != null && paramEps.Count > 0) {

					foreach (var paramEp in paramEps) {
						this.Endpoints.Add(EvaluationClient.CreateEndpoint(paramEp.Replace ("-ep:", "")));
					}
				} else {
					//nothing specified so use local host details
					var ipHostInfo = Dns.GetHostEntry (Dns.GetHostName ());
					var remoteEndPoint = new IPEndPoint (ipHostInfo.AddressList [0], defaultPort);
					Endpoints.Add (remoteEndPoint);
				}
			}

		}

		public List<IPEndPoint> Endpoints { get; set; }

	}
}


﻿using System;
using GAF;
using GAF.Net;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GAF.Extensions;
using GAF.Operators;
using System.Net.Sockets;
using System.Net;
using ConsumerFunctions;

namespace RemoteEvaluationClient
{
	public class Program
	{
		private const int RunCount = 1;
		private const int populationSize = 100;
		private static Parameters _settings;

		private static void Main (string[] args)
		{
			//get the command line parameters
			_settings = new Parameters (args);

			//get our cities 
			var cities = CreateCities ().ToList ();

			//Each city is an object the chromosome is a special case as it needs 
			//to contain each city only once. Therefore, our chromosome will contain 
			//all the cities with no duplicates

			//we can create an empty population as we will be creating the 
			//initial solutions manually.
			var population = new Population (true, false);
			population.OnEvaluationBegin += population_OnEvaluationBegin;

			//create the initial solutions (chromosomes)
			for (var p = 0; p < populationSize; p++) {

				var chromosome = new Chromosome ();
				foreach (var city in cities) {
					chromosome.Genes.Add (new Gene (city));
				}

				chromosome.Genes.ShuffleFast ();
				population.Solutions.Add (chromosome);
			}

			//create the elite operator 
			var elite = new Elite (5);

			//create crossover operator 
			var crossover = new Crossover (0.85) { CrossoverType = CrossoverType.DoublePointOrdered };

			//create the SwapMutate operator 
			var mutate = new SwapMutate (0.02);

			//get fitness function from a dll
			var cf = new Functions ("ConsumerFunctions.dll");
			var terminateFunction = cf.TerminateFunction; 

			// Locally defined fitness funtion
			var ga = new GeneticAlgorithm (population, null);

			//subscribe to the generation and run complete events 
			ga.OnGenerationComplete += ga_OnGenerationComplete;
			ga.OnRunComplete += ga_OnRunComplete;

			//add the operators 
			ga.Operators.Add (elite);
			ga.Operators.Add (crossover);
			ga.Operators.Add (mutate);

			//externally declared fitness function
			ga.Run (terminateFunction);

		}

		private static IEnumerable<City> CreateCities ()
		{
			var cities = new List<City> ();
			cities.Add (new City ("Birmingham", 52.486125, -1.890507));
			cities.Add (new City ("Bristol", 51.460852, -2.588139));
			cities.Add (new City ("London", 51.512161, -0.116215));
			cities.Add (new City ("Leeds", 53.803895, -1.549931));
			cities.Add (new City ("Manchester", 53.478239, -2.258549));
			cities.Add (new City ("Liverpool", 53.409532, -3.000126));
			cities.Add (new City ("Hull", 53.751959, -0.335941));
			cities.Add (new City ("Newcastle", 54.980766, -1.615849));
			cities.Add (new City ("Carlisle", 54.892406, -2.923222));
			cities.Add (new City ("Edinburgh", 55.958426, -3.186893));
			cities.Add (new City ("Glasgow", 55.862982, -4.263554));
			cities.Add (new City ("Cardiff", 51.488224, -3.186893));
			cities.Add (new City ("Swansea", 51.624837, -3.94495));
			cities.Add (new City ("Exeter", 50.726024, -3.543949));
			cities.Add (new City ("Falmouth", 50.152266, -5.065556));
			cities.Add (new City ("Canterbury", 51.289406, 1.075802));
			return cities;
		}

		private static void ga_OnRunComplete (object sender, GaEventArgs e)
		{
			var fittest = e.Population.GetTop (1) [0];
			foreach (var gene in fittest.Genes) {
				Console.WriteLine (((City)gene.ObjectValue).Name);
			}
		}

		private static void ga_OnGenerationComplete (object sender, GaEventArgs e)
		{
			var fittest = e.Population.GetTop (1) [0];

			var distanceToTravel = ConsumerFunctions.TravellingSalesman.CalculateDistance (fittest);
			Console.WriteLine (String.Format ("Generation: {0}, Evaluations: {1}, Fitness: {2}, Distance: {3}", 
				e.Generation,
				e.Evaluations,
				fittest.Fitness, 
				distanceToTravel)
			);
		}

		private static void population_OnEvaluationBegin (object sender, EvaluationEventArgs args)
		{
			try {

				var remoteEval = new EvaluationClient (_settings.Endpoints);
				remoteEval.OnEvaluationException += (object s, GaExceptionEventArgs e) => 
					Console.WriteLine (e.Message);

				var evaluations = remoteEval.Evaluate (args.SolutionsToEvaluate);

				if(evaluations > 0)
				{
					args.Evaluations = evaluations;
				}

				else{

					throw new ApplicationException("No evaluations undertaken, check that a server exists.");
				}

			} catch (Exception ex) {

				while (ex.InnerException != null) {
					ex = ex.InnerException;
				}
				throw;

			} finally {

				args.Cancel = true;
			}
		}
	}
}
